// to use
// to repeat thead, add this:
//
//  thead {
//  --pagedjs-table-repeat: repeat;
//  }
// https://gitlab.coko.foundation/pagedjs/pagedjs-extensions


class repeatElements extends Paged.Handler {
  constructor(chunker, polisher, caller) {
    super(chunker, polisher, caller);
    this.tableRepeat = [];
  }

  /* before anything. we check the declarations in the css to look for the pagedjs-table-repeat */
  onDeclaration(declaration, dItem, dList, rule) {
    // check if there is a property --pagedjs-table-repeat
    if (declaration.property == "--pagedjs-table-repeat") {
      // check the value
      console.log(declaration.value.value);

      let sel = csstree.generate(rule.ruleNode.prelude);
      console.log(sel);
      // if the declaration value == repeat
      // .exemple { --pagedjs-table-repeat: repeat;}
      if (declaration.value.value.trim() == "repeat") {
        let itemsList = sel.split(",");
        itemsList.forEach((elId) => {
          this.tableRepeat.push([elId, declaration.value.value.trim()]);
        });
      }
    }
  }

  /*
   * when we have the list of all the selector and the value they’re using,
   * we apply a set of class to them. so when they’re available on the page,
   * we can handle them
   *
   * */
  afterParsed(content) {
    this.tableRepeat.forEach((selector) => {
      content.querySelectorAll(selector[0]).forEach((el) => {
        el.classList.add("pagedjs-table-repeat");
        el.classList.add(`pagedjs-table-repeat-${selector[1]}`);
      });
    });
  }

  /* when we finish the page, we look where the break token is and find the parent previous thead. If there is one, we add it.*/

  finalizePage(pageFragment, page) {
    if (pageFragment.querySelectorAll("pagedjs-table-repeat")) {
      // make sure that you dont take the thead if it has a display none
      let repeatedElements = [...pageFragment
        .querySelectorAll(".pagedjs-table-repeat")]
        .filter((el) => {
        console.log(window.getComputedStyle(el).display)
          if (window.getComputedStyle(el).display != "none") return el;
        });
      console.log(repeatedElements)
      // get the last references of the latest block to find it in the source template
      let lastRepeatableElement = repeatedElements[repeatedElements.length - 1];

      // get the element from the source (from the template)
      const elementFromTemplate = this.chunker.source.querySelector(
        `[data-ref="${lastRepeatableElement.dataset.ref}"]`
      );

      //clone the last repeatable element
      let repeatedEl = elementFromTemplate.cloneNode(true);
      repeatedEl.dataset.ref = repeatedEl.dataset.ref + "-clone";

      //insert it just before the next element to put on the page
      elementFromTemplate.insertAdjacentElement("beforebegin", repeatedEl);

      //set the breaktoken on the repeated element
      if (lastRepeatableElement)
        if (page.endToken) {
          // /* move the break token*/
          page.breakToken = page.endToken.node = elementFromTemplate;
          page.breakToken = page.endToken.offset = 0;
        }

      // remove empty block from the end of the page
      /*check if the last tbody has a sibling otherwise remove it*/

      if (!lastRepeatableElement.nextElementSibling) {
        lastRepeatableElement.remove();
      }

      // if the last element is empty (textcontent ==  0), remove it.
      else if (
        lastRepeatableElement.nextElementSibling.textContent.length < 1
      ) {
        lastRepeatableElement.nextElementSibling.remove();
        lastRepeatableElement.remove();
      }
    }
  }
}

Paged.registerHandlers(repeatElements);
